/*
This program draws a line on an image if you click two points on the image
It displays the length in pixels of the line next to the image

*/
#include <cstdio>
#include <string>
#include <cmath>
#include <cairo.h>
#include <gtk/gtk.h>
#include <iostream>
#include "callbacks.h"
using namespace std;

static void do_drawing(cairo_t *);

struct gl{
  int count;
  double coordx[100];
  double coordy[100];
} glob;

//our image surface
cairo_surface_t *image; 

//when there is a draw event, do drawing
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){
  
  cr = gdk_cairo_create(gtk_widget_get_window(widget));
  do_drawing(cr);
  cairo_destroy(cr);

  return FALSE;
}


//calculate pixel length
double pixlength(double xo, double yo, double x, double y){
        double distance = sqrt((xo-x)*(xo-x)+(yo-y)*(yo-y));
        return distance;

}




//draws the lines and stuff
static void do_drawing(cairo_t *cr){
  //cairo context setup image
  image = cairo_image_surface_create_from_png("nw.png");
  
//   //Scale Image
//   
//       // calculate proportional scaling
//     int img_height = cairo_image_surface_get_height(image);
//     int img_width = cairo_image_surface_get_width(image);
//     float width_ratio = 600 / float(img_width);
//     float height_ratio = 600 / float(img_height);
//     float scale_xy = min(height_ratio, width_ratio);
//     // scale image and add it
//     cairo_save(cr);
//     //cairo_translate(cr,left, top);
//     cairo_scale(cr, scale_xy, scale_xy);
  cairo_set_source_surface(cr, image, 0, 0);
  cairo_paint(cr);

  //line settings
  cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
  cairo_set_line_width(cr, 2);

  //line stuff
  int i, j;
  for (i = 0; i <= glob.count - 1; i++ ) {
      for (j = 0; j <= glob.count - 1; j++ ) {
          cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
          cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
      }
  }
  
  glob.count = 0;
  //cairo_stroke(cr);

  //pixel print
 
  cairo_move_to(cr, glob.coordx[0], glob.coordy[0]);
  cairo_set_font_size(cr, 14);
  double distance= pixlength(glob.coordx[0], glob.coordy[0], glob.coordx[1], glob.coordy[1]);

  
char pixInfo[30];
sprintf(pixInfo, "Pixel length: %f", distance);  
  cairo_show_text(cr, pixInfo );
  cout<<"stuff"<<endl;
  cairo_stroke(cr);    
}

//event handler for mouse click
static gboolean clicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
  cout << "Clicked" << endl;  
  if (event->button == 1) {
        if (glob.count==1){
                gtk_widget_queue_draw(widget);
        }
        glob.coordx[glob.count] = event->x;
        glob.coordy[glob.count] = event->y;
        glob.count++;
        

    }

    if (event->button == 3) {
        gtk_widget_queue_draw(widget);
    }

    return TRUE;
}

//and finally, our main
int main(int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *darea;
  
  glob.count = 0;

  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  darea = gtk_drawing_area_new();
  gtk_widget_set_size_request(darea, 600, 600);
  //gtk_container_add(GTK_CONTAINER(window), darea);
 
  gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);

    //Make Buttons
  //draw lines
  GtkWidget *drawButton = gtk_button_new_with_label("Draw");
  g_signal_connect(G_OBJECT(drawButton), "button_press_event", G_CALLBACK(clickedDraw), NULL);  
  //Find objects
  GtkWidget *findButton = gtk_button_new_with_label("Find Object");
  g_signal_connect(G_OBJECT(findButton), "button_press_event", G_CALLBACK(clickedFind), NULL);  
  //Color to Greyscale
  GtkWidget *greyButton = gtk_button_new_with_label("Toggle Greyscale");
  g_signal_connect(G_OBJECT(greyButton), "button_press_event", G_CALLBACK(clickedGrey), NULL);  
  //Ruler
  GtkWidget *rulerButton = gtk_button_new_with_label("Display Ruler");
  g_signal_connect(G_OBJECT(rulerButton), "button_press_event", G_CALLBACK(clickedRuler), NULL);  
  
  
  //backing signals
  g_signal_connect_after(G_OBJECT(darea), "expose_event", G_CALLBACK(on_draw_event), NULL); 
  g_signal_connect(window, "destroy",G_CALLBACK(gtk_main_quit), NULL);  

  //event signals  
  g_signal_connect(window, "button-press-event", G_CALLBACK(clicked), NULL);
 
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 800, 600); 
  gtk_window_set_title(GTK_WINDOW(window), "Lines");

  
    //Make them Boxes and Start Packing
  GtkWidget *topHBox = gtk_hbox_new(FALSE, 0);
  GtkWidget *sideVBox = gtk_vbox_new(FALSE, 0);
  //add buttons to side bar
  gtk_box_pack_start(GTK_BOX (sideVBox), drawButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), findButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), greyButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), rulerButton, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX (topHBox), darea, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (topHBox), sideVBox, FALSE, FALSE, 0); 
  
  //Load it all
  gtk_container_add(GTK_CONTAINER(window), topHBox);
  gtk_widget_show(darea);
  gtk_widget_show(drawButton);
  gtk_widget_show(findButton);
  gtk_widget_show(greyButton);
  gtk_widget_show(rulerButton);
  gtk_widget_show(sideVBox);
  gtk_widget_show(topHBox);
  gtk_widget_show_all(window);
  
  
  
  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}