//Testing a pushButton

#include <iostream>
#include <gtk/gtk.h>

//(signal giver, what button press/release event triggered, data)
void ClickCallback(GtkWidget *widget, GdkEventButton *event, gpointer callback_data);

static void destroy_event(GtkWidget *widget, gpointer data);
static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);

int main(int argc,char* argv[]){
  //Initialize GTK
  gtk_init(&argc, &argv);
  
  //Declare pointers to widgetssss
  GtkWidget *window, *button;
  
  //Declare them widgizzles.
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  button = gtk_button_new_with_label("DON'T PANIC.");
  
  //Connect Callback
  g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(ClickCallback), NULL);
  //( relevant widge, signal to respond to, function to call)
  
  //Throw that widge atta window
  gtk_container_add(GTK_CONTAINER(window), button);
  
  //View that window
  gtk_widget_show_all(window);
  
  //Set up Shut down
  g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy_event), NULL);
  
  //wait for user input
  gtk_main();
  
  return 0;
}

void ClickCallback(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    // show which button was clicked
    std::cerr << "button pressed: " << event->button << std::endl;
  }
  
static void destroy_event(GtkWidget *widget, gpointer data){
    gtk_main_quit();
  }

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data){
    return FALSE; // must return false to trigger destroy event for window
  }