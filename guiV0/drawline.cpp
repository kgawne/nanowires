//to compile: g++ drawline.cpp -Os -s `pkg-config gtk+-3.0 --cflags` `pkg-config gtk+-3.0 --libs`
#include <cairo.h>
#include <gtk/gtk.h>
/* This program allows us to draw lines on an image
with chosen start and end points. We use cairo and gtk+ to do this
------------------------------------------------------------------*/

//initialize do_drawing 
static void do_drawing(cairo_t *, GtkWidget *widget);

//our image surface
cairo_surface_t *image; 

//our callback function to draw image
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){      
  cr = gdk_cairo_create(gtk_widget_get_window(widget));
  do_drawing(cr, widget);  
  cairo_destroy(cr);

  return FALSE;
}


//draws image
static void do_drawing(cairo_t *cr, GtkWidget *widget){

  cairo_set_source_surface(cr, image, 0, 0);
  cairo_paint(cr);
}

//loads image from file
static void load_image(){
        
  image = cairo_image_surface_create_from_png("nw.png"); 
}

//draws three black lines on the image
static void draw_lines(){ 
  cairo_t *ic;
  ic = cairo_create(image);
  
  cairo_set_source_rgb(ic, 0.0 , 0.0 , 0.0);//to make it black
  cairo_move_to(ic, 20, 30);//first line
  cairo_line_to(ic,100,500);

  cairo_move_to(ic, 20, 30);//second line
  cairo_line_to(ic,500,300);

  cairo_move_to(ic, 40, 80);//third line
  cairo_line_to(ic,100,200);

  cairo_stroke(ic);   
}

int main (int argc, char *argv[]){

  GtkWidget *window;
  GtkWidget *darea;
  
  load_image();
  draw_lines();

  //our init
  gtk_init(&argc, &argv);

  //define our window
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  //define our drawing area
  darea = gtk_drawing_area_new();

  //pack the drawing area into the window
  gtk_container_add(GTK_CONTAINER (window), darea);
	

  g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL); 
 
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

  //customize our window
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 500, 500); 
  gtk_window_set_title(GTK_WINDOW(window), "Lines");

  //show everything
  gtk_widget_show_all(window);

  gtk_main();
  
  //clean up
  cairo_surface_destroy(image);

  return 0;
}
