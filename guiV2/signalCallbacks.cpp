// Implementation for signal callbacks.
#include <iostream>
#include <gtk/gtk.h>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cairo.h>
#include "globals.h"
#include "drawing.h"
#include "signalCallbacks.h"
using namespace std;

extern GtkWidget * darea;

void clearScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){

	cairo_t *cr;
	cr = gdk_cairo_create(gtk_widget_get_window(darea));
	image = cairo_image_surface_create_from_png("nw.png");
	
  //Scale Image
  
      // calculate proportional scaling
    int img_height = cairo_image_surface_get_height(image);
    int img_width = cairo_image_surface_get_width(image);
    float width_ratio = 600 / float(img_width);
    float height_ratio = 600 / float(img_height);
    float scale_xy = min(height_ratio, width_ratio);
    // scale image and add it
    cairo_save(cr);
    cairo_translate(cr,0,0);
    cairo_matrix_t matrix;
    cairo_matrix_init_scale (&matrix, 1/scale_xy, 1/scale_xy);
    cairo_pattern_t* pattern = cairo_pattern_create_for_surface(image);
    cairo_pattern_set_matrix(pattern, &matrix);
    cairo_pattern_set_filter(pattern, CAIRO_FILTER_FAST);
  //cairo_scale(cr, scale_xy, scale_xy);
    cairo_set_source(cr, pattern);
	//cairo_set_source_surface(cr, image, 0, 0);
	cairo_paint(cr);
	
}

//saves the drawings on the screen
void saveScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  
  GError *error = NULL;
      //cairo_pattern_create_for_surface
    	
  GdkPixbuf *screenshot;
	GdkPixbuf *subscreen;

    	GdkWindow *root_window;
    	gint x_orig, y_orig;
    	gint width, height;
    	root_window = gdk_get_default_root_window ();
    	gdk_drawable_get_size (root_window, &width, &height);      
   	gdk_window_get_origin (root_window, &x_orig, &y_orig);

	screenshot = gdk_pixbuf_get_from_drawable (NULL, root_window, NULL,
                                           x_orig, y_orig, 0, 0, width, height);
	subscreen= gdk_pixbuf_new_subpixbuf(screenshot, 303 ,110 ,600 ,560 );

    	gdk_pixbuf_save(subscreen, "nwedit", "jpeg", &error, "quality", "100", NULL);
}

//event handler for mouse click
void clicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data){
  
  if(glob.rulerTog.getStatus()){  
    if (event->x < 600 && event->y < 600){  



      if (event->button == 1) {//for left button
	if (glob.getCount()==1){ //after the user has clicked two points
	  gtk_widget_queue_draw(widget);//do the drawing
	}
	
	glob.rulerPts[glob.getCount()].setX( event->x);//store x location in global glob
	glob.rulerPts[glob.getCount()].setY( event->y);//store y location in global glob
	glob.setCount(glob.getCount()+1);//increment count
      }
 

        if (event->button == 3) {//for right button
	       gtk_widget_queue_draw(widget);//do the drawing
        }
    }
  }


  if(glob.drawTog.getStatus()){//if find button is toggled
   if (event->x < 600 && event->y < 600){ 
	if (event->button==1){//for left button
	   glob.circlePts[glob.getCount()].setX( event->x);//store the click x positon
	   glob.circlePts[glob.getCount()].setY( event->y);//store the click y position
           glob.setCount(glob.getCount()+1); //increment
	   gtk_widget_queue_draw(widget);//do the drawing
	}
   }
  }
	
  return;
}

//when there is a draw event, do drawing
void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){
  cr = gdk_cairo_create(gtk_widget_get_window(widget));//cairo context containing widget
  do_drawing(cr); //draw on the context
  cairo_destroy(cr);

  return;
}

void scaleEdited(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  
  const gchar * text = gtk_entry_get_text(GTK_ENTRY(widget));
  
  for(int i = 0; i < strlen(text); i++){
    if(!isdigit(text[i])){
      glob.setScale( 0);
//       cout << "Scale: " << glob.getScale()<<endl;
      return;
    }
  }
  glob.setScale( atoi(text));
//   cout << "Scale: " << glob.getScale()<<endl;
}


//prints message in command line if button toggled/untoggled
void clickedDraw(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  
  glob.drawTog.setStatus( GTK_TOGGLE_BUTTON(widget)->active);
  
  if(glob.drawTog.getStatus()){
    std::cerr << " Draw Line toggled" << std::endl;
  }else{
    std::cerr << " Draw Line untoggled" << std::endl;
  }
}

//prints message in command line if button toggled/untoggled
void clickedRuler(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.rulerTog.setStatus( GTK_TOGGLE_BUTTON(widget)->active);
  if(glob.rulerTog.getStatus()){
    std::cerr << " Ruler toggled" << std::endl;
  }else{
    std::cerr << " Ruler untoggled" << std::endl;
  }
}
