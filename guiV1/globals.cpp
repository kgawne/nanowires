//Kelly D Gawne, globals.cpp

// Defines member functions for globals and subclasses.

#include <iostream>
#include "globals.h"
using namespace std;

//Default Constructor for global
Global::Global(){
  count = 0;
  scale = 0;
}

void Global::setScale(int k){scale = k;}
int Global::getScale(void){ return scale;}

void Global::setCount(int k){count = k;}
int Global::getCount(void){ return count;}

Point::Point(){}

void Point::setX(int k){x = k;}
int Point::getX(void){ return x;}

void Point::setY(int k){y = k;}
int Point::getY(void){ return y;}

TogStat::TogStat(){};

void Global::setStatus(int k){status = k;}
int Global::getStatus(void){ return status;}