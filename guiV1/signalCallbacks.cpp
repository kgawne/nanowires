// Implementation for signal callbacks.
#include <iostream>
#include <gtk/gtk.h>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cairo.h>
#include "signalCallbacks.h"
using namespace std;

void clearScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){

	cairo_t *cr;
	cr = gdk_cairo_create(gtk_widget_get_window(darea));
	image = cairo_image_surface_create_from_png("nw.png");
	cairo_set_source_surface(cr, image, 0, 0);
	cairo_paint(cr);

	//gtk_widget_queue_draw(widget);
	
	//cairo_destroy(cr);
	/*cr = gdk_cairo_create(gtk_widget_get_window(widget));
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_paint (cr);*/
	

	/*cr = gdk_cairo_create(gtk_widget_get_window(widget));
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_paint (cr);
	cairo_destroy (cr);*/

	/*cairo_t *cr;
	cr = gdk_cairo_create(gtk_widget_get_window(widget));
	image = cairo_image_surface_create_from_png("nw.png");
	cairo_set_source_surface(cr, image, 0, 0);
	cairo_paint(cr);*/
	
}

//saves the drawings on the screen
void saveScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){

	//GdkRectangle *rect(0,0,50,50);
	GError *error = NULL;

    	GdkPixbuf *screenshot;
	GdkPixbuf *subscreen;
	

    	GdkWindow *root_window;
    	gint x_orig, y_orig;
    	gint width, height;
    	root_window = gdk_get_default_root_window ();
    	gdk_drawable_get_size (root_window, &width, &height);      
   	gdk_window_get_origin (root_window, &x_orig, &y_orig);

    	screenshot = gdk_pixbuf_get_from_drawable (NULL, root_window, NULL,
                                           x_orig, y_orig, 0, 0, width, height);
	subscreen= gdk_pixbuf_new_subpixbuf(screenshot, 303 ,110 ,600 ,560 );

        gdk_pixbuf_save(subscreen, "nwedit", "jpeg", &error, "quality", "100", NULL);


}

//event handler for mouse click
static gboolean clicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data){
  if(glob.toggledRuler){  
    if (event->x < 600 && event->y < 600){  
      if (event->button == 1) {
	if (glob.count==1){
	  gtk_widget_queue_draw(widget);
	}
	//Store the location in global glob
	glob.coordx[glob.count] = event->x;
	glob.coordy[glob.count] = event->y;
	glob.count++;
      }

      if (event->button == 3) {
	      gtk_widget_queue_draw(widget);
      }
    }
  }
  return TRUE;
}

//when there is a draw event, do drawing
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){

  cr = gdk_cairo_create(gtk_widget_get_window(widget));
  do_drawing(cr);
  cairo_destroy(cr);

  return FALSE;
}

void scaleEdited(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  
  const gchar * text = gtk_entry_get_text(GTK_ENTRY(widget));
  
  for(int i = 0; i < strlen(text); i++){
    if(!isdigit(text[i])){
      glob.scale = 0;
//       cout << "Scale: " << glob.scale<<endl;
      return;
    }
  }
  glob.scale = atoi(text);
//   cout << "Scale: " << glob.scale<<endl;
}

void clickedDraw(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  
  glob.toggledDraw = GTK_TOGGLE_BUTTON(widget)->active;
  
  if(glob.toggledDraw){
    std::cerr << " Draw Line toggled" << std::endl;
  }else{
    std::cerr << " Draw Line untoggled" << std::endl;
  }
}

void clickedFind(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledFind = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledFind){
    std::cerr << " Find toggled" << std::endl;
  }else{
    std::cerr << " Find untoggled" << std::endl;
  }
}

void clickedGrey(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledGrey = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledGrey){
    std::cerr << " grey toggled" << std::endl;
  }else{
    std::cerr << " grey untoggled" << std::endl;
  }
}

void clickedRuler(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledRuler = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledRuler){
    std::cerr << " Ruler toggled" << std::endl;
  }else{
    std::cerr << " Ruler untoggled" << std::endl;
  }
}
