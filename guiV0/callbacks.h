//guiBase's callbacks

#ifndef CALLBACKS_H
#define CALLBACKS_H
#include <iostream>
#include <gtk/gtk.h>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cairo.h>
using namespace std;

extern GtkWidget *darea;

//our image surface
cairo_surface_t *image; 

static void do_drawing(cairo_t *);


//functions for our push buttons(clear and save)
//clears the drawings from the screen
void clearScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){

	cairo_t *cr;
	cr = gdk_cairo_create(gtk_widget_get_window(darea));
	image = cairo_image_surface_create_from_png("nw.png");
	cairo_set_source_surface(cr, image, 0, 0);
	cairo_paint(cr);

	//gtk_widget_queue_draw(widget);
	
	//cairo_destroy(cr);
	/*cr = gdk_cairo_create(gtk_widget_get_window(widget));
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_paint (cr);*/
	

	/*cr = gdk_cairo_create(gtk_widget_get_window(widget));
	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_paint (cr);
	cairo_destroy (cr);*/

	/*cairo_t *cr;
	cr = gdk_cairo_create(gtk_widget_get_window(widget));
	image = cairo_image_surface_create_from_png("nw.png");
	cairo_set_source_surface(cr, image, 0, 0);
	cairo_paint(cr);*/
	
}

//saves the drawings on the screen
void saveScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){

	//GdkRectangle *rect(0,0,50,50);
	GError *error = NULL;

    	GdkPixbuf *screenshot;
	GdkPixbuf *subscreen;
	

    	GdkWindow *root_window;
    	gint x_orig, y_orig;
    	gint width, height;
    	root_window = gdk_get_default_root_window ();
    	gdk_drawable_get_size (root_window, &width, &height);      
   	gdk_window_get_origin (root_window, &x_orig, &y_orig);

    	screenshot = gdk_pixbuf_get_from_drawable (NULL, root_window, NULL,
                                           x_orig, y_orig, 0, 0, width, height);
	subscreen= gdk_pixbuf_new_subpixbuf(screenshot, 303 ,110 ,600 ,560 );

        gdk_pixbuf_save(subscreen, "nwedit", "jpeg", &error, "quality", "100", NULL);


}





//functions and other important stuff for ruler
struct gl{
	int count;
	double coordx[100];
  	double coordy[100];
	double rectcoordx[500];
	double rectcoordy[500];
	double circleCenterx[100];
	double circleCentery[100];
	int toggledDraw;
	int toggledFind;
	int toggledGrey;
	int toggledRuler;
	int scale;
} glob;

//Backing pixmap for drawing area
static GdkPixmap *pixmap = NULL;

void scaleEdited(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  const gchar * text = gtk_entry_get_text(GTK_ENTRY(widget));
  for(int i = 0; i < strlen(text); i++){
    if(!isdigit(text[i])){
      glob.scale = 0;
//       cout << "Scale: " << glob.scale<<endl;

      return;
    }
  }
  glob.scale = atoi(text);
//   cout << "Scale: " << glob.scale<<endl;
}

void clickedDraw(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledDraw = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledDraw){
    std::cerr << " Draw Line toggled" << std::endl;
  }else{
    std::cerr << " Draw Line untoggled" << std::endl;
  }
}

void clickedFind(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledFind = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledFind){
    std::cerr << " Find toggled" << std::endl;
  }else{
    std::cerr << " Find untoggled" << std::endl;
  }
}

void clickedGrey(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledGrey = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledGrey){
    std::cerr << " grey toggled" << std::endl;
  }else{
    std::cerr << " grey untoggled" << std::endl;
  }
}

void clickedRuler(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
  glob.toggledRuler = GTK_TOGGLE_BUTTON(widget)->active;
  if(glob.toggledRuler){
    std::cerr << " Ruler toggled" << std::endl;
  }else{
    std::cerr << " Ruler untoggled" << std::endl;
  }
}







//when there is a draw event, do drawing
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){
  
	cr = gdk_cairo_create(gtk_widget_get_window(widget));
	do_drawing(cr);
	cairo_destroy(cr);
  	return FALSE;
}
 
//configure event for backing pixmap
static gboolean configure_event( GtkWidget         *widget,
                                 GdkEventConfigure *event ){


	if (pixmap)
		g_object_unref (pixmap);

	pixmap = gdk_pixmap_new (widget->window, widget->allocation.width, widget->allocation.height, -1);
	gdk_draw_rectangle (pixmap, widget->style->white_gc, TRUE, 0, 0, widget->allocation.width, widget->allocation.height);


	GError *error = NULL;
    	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file ("nw.png", &error);
	gdk_pixbuf_render_pixmap_and_mask (pixbuf, &pixmap, NULL, 0);
	


  	return TRUE;
}




//calculate pixel length
double pixlength(double xo, double yo, double x, double y){
	double distance = sqrt((xo-x)*(xo-x)+(yo-y)*(yo-y));
	return distance;

}

/*static void draw_brush( GtkWidget *widget,
                        gdouble    x,
                        gdouble    y){
 
	GdkRectangle update_rect;


	update_rect.x = x - 5;
	update_rect.y = y - 5;
	update_rect.width = 2;
	update_rect.height = 2;
	gdk_draw_rectangle (pixmap,
		      widget->style->white_gc,
		      TRUE,
		      update_rect.x, update_rect.y,
		      update_rect.width, update_rect.height);
	gtk_widget_queue_draw_area (widget, 
		              update_rect.x, update_rect.y,
		              update_rect.width, update_rect.height);
}*/


//draws the lines and stuff
static void do_drawing(cairo_t *cr){
	//cairo context setup image
  image = cairo_image_surface_create_from_png("nw.png");
  
  //Scale Image
  
      // calculate proportional scaling
    int img_height = cairo_image_surface_get_height(image);
    int img_width = cairo_image_surface_get_width(image);
    float width_ratio = 600 / float(img_width);
    float height_ratio = 600 / float(img_height);
    float scale_xy = min(height_ratio, width_ratio);
    // scale image and add it
    cairo_save(cr);
    cairo_translate(cr,0,0);
    cairo_matrix_t matrix;
    cairo_matrix_init_scale (&matrix, 1/scale_xy, 1/scale_xy);
    cairo_pattern_t* pattern = cairo_pattern_create_for_surface(image);
    cairo_pattern_set_matrix(pattern, &matrix);
    cairo_pattern_set_filter(pattern, CAIRO_FILTER_FAST);
    //cairo_scale(cr, scale_xy, scale_xy);
    cairo_set_source(cr, pattern);
    //     cairo_set_source_surface(cr, image, 0, 0);
    cairo_paint(cr);

  if(glob.toggledRuler){
	//line settings
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
	cairo_set_line_width(cr, 2);

	//line stuff
	int i, j;
	for (i = 0; i <= glob.count - 1; i++ ) {
		for (j = 0; j <= glob.count - 1; j++ ) {
		cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
		cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
		}
	}
  
  	glob.count = 0;
  	cairo_stroke(cr);

	cairo_move_to(cr, glob.coordx[0], glob.coordy[0]);
	cairo_set_font_size(cr, 16);
	double distance= pixlength(glob.coordx[0], glob.coordy[0], glob.coordx[1], glob.coordy[1]);

	char pixInfo[30];
	
	if (glob.scale == 0){
	  sprintf(pixInfo, "%.2f Pixels", distance); 
	}else{
	  sprintf(pixInfo, "%.2f nm", distance/glob.scale); 
	}
	cairo_select_font_face(cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
	cairo_show_text(cr, pixInfo );
	//cairo_stroke(cr); 
  }


   if (glob.toggledDraw){

	cairo_set_line_width(cr, 5);
	for (int k=0; k< glob.count ; k++){
		cairo_rectangle(cr, glob.rectcoordx[k],glob.rectcoordy[k],2 , 2);
		cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
		cairo_fill(cr);
	}
	
	
    }

   


   if (glob.toggledFind){
	//glob.count=0;
	
	cairo_set_line_width(cr, 5);
	for (int k=0; k< glob.count ; k++){
		cairo_arc(cr, glob.circleCenterx[k], glob.circleCentery[k], 8, 0, 2 * M_PI);
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
	cairo_stroke_preserve(cr);
	cairo_set_source_rgb(cr, 0.3, 0.1, 0.6);
	cairo_fill(cr);
	
	}
	
	//glob.count=0;


   }
}



//event handler for mouse click
static gboolean clicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data){
    if (event->x < 600 && event->y < 600){  
     if (glob.toggledRuler){      
	if (event->button == 1) {
		if (glob.count==1){
	  		gtk_widget_queue_draw(widget);
		}
	//Store the location in global glob
	glob.coordx[glob.count] = event->x;
	glob.coordy[glob.count] = event->y;
	glob.count++;
        }

       if (event->button == 3) {
	      gtk_widget_queue_draw(widget);
       }
    }




   if (glob.toggledDraw){
      if (event->button ==1){
		glob.rectcoordx[glob.count] = event->x;
		glob.rectcoordy[glob.count] = event->y;
		glob.count++;
		gtk_widget_queue_draw(widget);
		
      }

   }

  if (glob.toggledFind){
	if (event->button==1){
	glob.circleCenterx[glob.count] = event->x;
	glob.circleCentery[glob.count] = event->y;
        glob.count++;
	gtk_widget_queue_draw(widget);
	

	}
   }

  }


  return TRUE;
}

//event handler for motion
static gboolean dragged(GtkWidget *widget, GdkEventMotion *event, gpointer user_data){
   cout<< "being dragged"<<endl;
   if (event->x < 600 && event->y < 600){
      if(glob.toggledDraw){
	
		int x, y;
		GdkModifierType state;

		if (event->is_hint)
			gdk_window_get_pointer (event->window, &x, &y, &state);
		else{

			x = event->x;
			y = event->y;
 			//state = event->state;
		}

			//glob.rectcoordx[glob.count] = event->x;
			//glob.rectcoordy[glob.count] = event->y;
			//glob.count++;
			//gtk_widget_queue_draw(widget);


		if (state & GDK_BUTTON1_MASK){
			glob.rectcoordx[glob.count] = event->x;
			glob.rectcoordy[glob.count] = event->y;
			glob.count++;
			gtk_widget_queue_draw(widget);
		}
  
  
			return TRUE;



	   }
	
		


   }
}

#endif
