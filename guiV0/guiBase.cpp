//Packing boxes for a GUI

#include <iostream>
#include <gtk/gtk.h>
#include "callbacks.h"
//#include <cmath>

static void destroy_event(GtkWidget *widget, gpointer data){
    gtk_main_quit();
  }

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data){
    return FALSE; // must return false to trigger destroy event for window
  }

int main(int argc,char* argv[]){
  //Initialize GTK
  gtk_init(&argc, &argv);
  
  //Make window
  GtkWidget  *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  //Name it "Nanowires Galore"
  gtk_window_set_title (GTK_WINDOW(window), "Nanowires Galore");
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 600); 

  //Make viewspace
  GtkWidget *imageView = gtk_image_new_from_file("nw.png");
  
  //Make Buttons
  //draw lines
  GtkWidget *drawButton = gtk_button_new_with_label("Draw");
  g_signal_connect(G_OBJECT(drawButton), "button_press_event", G_CALLBACK(clickedDraw), NULL);  
  //Find objects
  GtkWidget *findButton = gtk_button_new_with_label("Find Object");
  g_signal_connect(G_OBJECT(findButton), "button_press_event", G_CALLBACK(clickedFind), NULL);  
  //Color to Greyscale
  GtkWidget *greyButton = gtk_button_new_with_label("Toggle Greyscale");
  g_signal_connect(G_OBJECT(greyButton), "button_press_event", G_CALLBACK(clickedGrey), NULL);  
  //Ruler
  GtkWidget *rulerButton = gtk_button_new_with_label("Display Ruler");
  g_signal_connect(G_OBJECT(rulerButton), "button_press_event", G_CALLBACK(clickedRuler), NULL);  

  //Make them Boxes and Start Packing
  GtkWidget *topHBox = gtk_hbox_new(FALSE, 0);
  GtkWidget *sideVBox = gtk_vbox_new(FALSE, 0);
  //add buttons to side bar
  gtk_box_pack_start(GTK_BOX (sideVBox), drawButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), findButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), greyButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), rulerButton, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX (topHBox), imageView, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (topHBox), sideVBox, FALSE, FALSE, 0); 
  
  //Load it all
  gtk_container_add(GTK_CONTAINER(window), topHBox);
  gtk_widget_show(imageView);
  gtk_widget_show(drawButton);
  gtk_widget_show(findButton);
  gtk_widget_show(greyButton);
  gtk_widget_show(rulerButton);
  gtk_widget_show(sideVBox);
  gtk_widget_show(topHBox);
  gtk_widget_show_all(window);
  
  //Clean exit
<<<<<<< HEAD
 // g_signal_connect(window, "destroy", G_CALLBACK(gtk_exit),NULL);
=======
  //g_signal_connect(window, "destroy", G_CALLBACK(gtk_exit),NULL);
  g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy_event), NULL);
>>>>>>> c7ef4e4eda123a11e3dc232305a46bf986a99ccc
 
  //Begin user interaction
  gtk_main();
  
  return 0;
}
