//A header file for drawing functions

#ifndef DRAWING_H
#define DRAWING_H
#include <iostream>
#include <gtk/gtk.h>
#include <cairo.h>
using namespace std;

  //our image surface
  extern cairo_surface_t *image;

//calculates length in pixels
double pixlength(double xo, double yo, double x, double y);

//contains the drawing capabilities for all of our buttons
void do_drawing(cairo_t *);



#endif
