//guiBase2's callbacks

#ifndef CALLBACKS2_H
#define CALLBACKS2_H

//#include <cairo.h>
#include <iostream>
#include <gtk/gtk.h>

//Necessary Functions for window control
static void destroy_event(GtkWidget *widget, gpointer data){
    gtk_main_quit();
  }

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data){
    return FALSE; // must return false to trigger destroy event for window
  }


//Actual Callbacks

void clickedDraw(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    std::cerr << "Pressed Draw Line." << std::endl;
}

void clickedFind(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    std::cerr << "Pressed Find Object." << std::endl;
}

void clickedGrey(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    std::cerr << "Pressed Toggle Greyscale." << std::endl;
}

void clickedRuler(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    std::cerr << "Pressed Display Ruler." << std::endl;
}


#endif