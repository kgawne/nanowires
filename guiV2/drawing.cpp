// Implementation for drawing functions
#include <cstdio>
#include <string>
#include <cmath>
#include <cairo.h>
#include <gtk/gtk.h>
#include <iostream>
#include "globals.h"
//#include "signalCallbacks.h"
#include "drawing.h"
using namespace std;

  //our image surface
  cairo_surface_t *image;

//calculate distance in pixels for line drawing
double pixlength(double xo, double yo, double x, double y){
	//use the distance formula
	double distance = sqrt((xo-x)*(xo-x)+(yo-y)*(yo-y)); 
  	return distance;
}



// cairo_surface_t* scaleImage(char * filename){
//   GdkPixbuf *pixbuf;
//   GError *error = NULL;
//   pixbuf = gdk_pixbuf_new_from_file_at_size(filename, 600,600,&error);
//   if (!pixbuf){
//     printf("Error: %s\n", error->message);
//     g_error_free(error);
//     return 0;
//   }
//   gdk_pixbuf_save("Temp", "png", NULL, "quality","100", NULL);
// }




//draws different things depending on which button is toggled
void do_drawing(cairo_t *cr){
   //cairo context setup image
   image = cairo_image_surface_create_from_png("nw.png");
  
  //Scale Image
  
      // calculate proportional scaling
    int img_height = cairo_image_surface_get_height(image);
    int img_width = cairo_image_surface_get_width(image);
    float width_ratio = 600 / float(img_width);
    float height_ratio = 600 / float(img_height);
    float scale_xy = min(height_ratio, width_ratio);
    // scale image and add it
    cairo_save(cr);
    cairo_translate(cr,0,0);
    cairo_matrix_t matrix;
    cairo_matrix_init_scale (&matrix, 1/scale_xy, 1/scale_xy);
    cairo_pattern_t* pattern = cairo_pattern_create_for_surface(image);
    cairo_pattern_set_matrix(pattern, &matrix);
    cairo_pattern_set_filter(pattern, CAIRO_FILTER_FAST);
  //cairo_scale(cr, scale_xy, scale_xy);
cairo_set_source(cr, pattern);
    
   // cairo_set_source_surface(cr, image, 0, 0);//put image in cairo context
    cairo_paint(cr);//show the context

  if(glob.rulerTog.getStatus()){//if ruler button is toggled
    //line settings
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
	cairo_set_line_width(cr, 2);

	//loop through coordx and coordj arrays to draw a line between two points
	int i, j;
	for (i = 0; i <= glob.getCount() - 1; i++ ) {
		for (j = 0; j <= glob.getCount() - 1; j++ ) {
		cairo_move_to(cr, glob.rulerPts[i].getX(), glob.rulerPts[i].getY());
		cairo_line_to(cr, glob.rulerPts[j].getX(), glob.rulerPts[j].getY());
		}
	}
        
	glob.setCount( 0);//reset line to 0
	cairo_stroke(cr);//put the line on the cairo context

	//pixel print

	cairo_move_to(cr, glob.rulerPts[0].getX(), glob.rulerPts[0].getY());//we want the distance displayed at the line origin
	cairo_set_font_size(cr, 16);
	double distance= pixlength(glob.rulerPts[0].getX(), glob.rulerPts[0].getY(),
				   glob.rulerPts[1].getX(), glob.rulerPts[1].getY());
	
	char pixInfo[30];
	
	if (glob.getScale() == 0){
	  sprintf(pixInfo, "%.2f Pixels", distance); //we put the pixel distance into an array that we show on the screen
	}else{
	  sprintf(pixInfo, "%.2f nm", distance/glob.getScale()); //we put the converted distance into array we show on screen
	}
	cairo_select_font_face(cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);//set font attributes
	cairo_show_text(cr, pixInfo );//show text
	
  }

   if (glob.drawTog.getStatus()){//if find button is toggled
        cairo_set_line_width(cr, 5);

	//draw all of the circles, whose positions are in the circlecenter arrays
	for (int k=0; k< glob.getCount() ; k++){
	cairo_arc(cr, glob.circlePts[k].getX(), glob.circlePts[k].getY(), 8, 0, 2 * M_PI);//draw the circle
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
	cairo_stroke_preserve(cr);//make the outline
	cairo_set_source_rgb(cr, 0.3, 0.1, 0.6);
	cairo_fill(cr);//fill the middle of the circle
	}
   }
}


