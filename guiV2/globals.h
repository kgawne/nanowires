//A header file for the global classes

#ifndef GLOBALS_H
#define GLOBALS_H
#include <iostream>
#include <gtk/gtk.h>
#include <cairo.h>
using namespace std;


class Point{
  public:
    Point(); //Default constructor
    
    void setX(int);
    int getX(void);

    void setY(int);
    int getY(void);
   
  private:
    double x;
    double y;   
};

class TogStat{
  public:
    TogStat(); //Default constructor
    
    void setStatus(int);
    int getStatus(void);
   
  private:
    int status;   
};

class Global{
  public:
    Global(); //Default constructor
    
    void setScale(int);
    int getScale(void);

    void setCount(int);
    int getCount(void);
    
    Point rulerPts[100];
    Point circlePts[100];
    TogStat drawTog;
    TogStat rulerTog;  
    
  private:
    int count;
   int scale;
  
};
  
  extern Global glob;
  extern GtkWidget *darea;

#endif