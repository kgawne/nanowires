/*
This program draws a line on an image if you click two points on the image
It displays the length in pixels of the line next to the image

*/
#include <cstdio>
#include <string>
#include <cmath>
#include <cairo.h>
#include <gtk/gtk.h>
#include <iostream>
#include "globals.h"
#include "signalCallbacks.h"
#include "drawing.h"
using namespace std;

int main(int argc, char *argv[])
{
  
  glob.setCount(0);

  gtk_init(&argc, &argv); //start gtk processing
  
//Initialize Widgets
  extern GtkWidget *darea;
  GtkWidget *window, *drawButton, *rulerButton;
  GtkWidget *saveButton, *clearButton;
  GtkWidget *scaleLabel, *scaleBox, *topHBox, *sideVBox;
  
//Define widgets
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Nanowires");
    gtk_window_set_default_size(GTK_WINDOW(window), 750, 600); 
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);

  darea = gtk_drawing_area_new();
    gtk_widget_set_size_request(darea, 600, 600);
  
  
  drawButton = gtk_toggle_button_new_with_label("Draw circle");
  rulerButton = gtk_toggle_button_new_with_label("Display Ruler");
  saveButton = gtk_button_new_with_label("Save screen");
  clearButton = gtk_button_new_with_label("Clear all");
  
  scaleLabel = gtk_label_new("Scale in pixels/nm");
  scaleBox = gtk_entry_new_with_max_length( 6);

  topHBox = gtk_hbox_new(FALSE, 0);
  sideVBox = gtk_vbox_new(FALSE, 0);
  
//Connect Signals to Their Callbacks

  g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_draw_event), NULL); 
  g_signal_connect(window, "button-press-event", G_CALLBACK(clicked), NULL);
  g_signal_connect(G_OBJECT(scaleBox), "changed", G_CALLBACK(scaleEdited), NULL);
  g_signal_connect(G_OBJECT(drawButton), "toggled", G_CALLBACK(clickedDraw), NULL);  
  g_signal_connect(G_OBJECT(rulerButton), "toggled", G_CALLBACK(clickedRuler), NULL);  
  g_signal_connect(G_OBJECT(saveButton), "button-press-event", G_CALLBACK(saveScreen), NULL);
  g_signal_connect(G_OBJECT(clearButton), "button-press-event", G_CALLBACK(clearScreen), NULL);
  
  //For a clean exit
  g_signal_connect(window, "destroy",G_CALLBACK(gtk_main_quit), NULL);  
  
// Pack Boxes

  //add buttons to side bar
  gtk_box_pack_start(GTK_BOX (sideVBox), drawButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), rulerButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), saveButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), clearButton, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), scaleLabel, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (sideVBox), scaleBox, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX (topHBox), darea, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (topHBox), sideVBox, FALSE, FALSE, 0); 
  
//Load it all
  gtk_container_add(GTK_CONTAINER(window), topHBox);
  gtk_widget_show(darea);
  gtk_widget_show(drawButton);
  gtk_widget_show(rulerButton);
  gtk_widget_show(saveButton);
  gtk_widget_show(clearButton);
  gtk_widget_show(scaleBox);
  gtk_widget_show(scaleLabel);
  gtk_widget_show(sideVBox);
  gtk_widget_show(topHBox);
  
  gtk_widget_show_all(window);

// Begin Interaction
  gtk_main();

  return 0;
}