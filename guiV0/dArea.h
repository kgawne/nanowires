//guiBase2's cairo business

#ifndef DAREA_H
#define DAREA_H

#include <cairo.h>
#include <string>
#include <cmath>
#include <iostream>
#include <gtk/gtk.h>
using namespace std;

//Drawing Area Business
struct gl{
  int count;
  double coordx[100];
  double coordy[100];
} glob;

//our image surface
cairo_surface_t *image; 

static void do_drawing(cairo_t *);

//when there is a draw event, do drawing
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data){
  
  cr = gdk_cairo_create(gtk_widget_get_window(widget));
  do_drawing(cr);
  cairo_destroy(cr);

    cout<<"finished on draw event" << endl;
  return FALSE;
}

//calculate pixel length
double pixlength(double xo, double yo, double x, double y){
        double distance = sqrt((xo-x)*(xo-x)+(yo-y)*(yo-y));
        return distance;

}

//draws the lines and stuff
static void do_drawing(cairo_t *cr){
  //cairo context setup image
  image = cairo_image_surface_create_from_png("nw.png");
  cairo_set_source_surface(cr, image, 0, 0);
//   cairo_paint(cr);
// 
//   //line settings
//   cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
//   cairo_set_line_width(cr, 2);
// 
//   //line stuff
//   int i, j;
//   for (i = 0; i <= glob.count - 1; i++ ) {
//       for (j = 0; j <= glob.count - 1; j++ ) {
//           cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
//           cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
//       }
//   }
//   
//   glob.count = 0;
//   cairo_move_to(cr, glob.coordx[0], glob.coordy[0]);
//   cairo_set_font_size(cr, 14);
//   double distance= pixlength(glob.coordx[0], glob.coordy[0], glob.coordx[1], glob.coordy[1]);
// 
//   
//   char pixInfo[30];
//   sprintf(pixInfo, "Pixel length: %d", distance);  
//   cairo_show_text(cr, pixInfo );
//   cout<<"stuff"<<endl;
  cairo_stroke(cr);    
}

//event handler for mouse click
static gboolean clicked(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    if (event->button == 1) {
        if (glob.count==1){
                gtk_widget_queue_draw(widget);
        }
        glob.coordx[glob.count] = event->x;
        glob.coordy[glob.count] = event->y;
        glob.count++;
        

    }

    if (event->button == 3) {
        gtk_widget_queue_draw(widget);
    }

    return TRUE;
}


#endif