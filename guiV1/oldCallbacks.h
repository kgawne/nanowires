//guiBase's callbacks

#ifndef CALLBACKS_H
#define CALLBACKS_H
#include <iostream>
#include <gtk/gtk.h>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cairo.h>
using namespace std;

//functions and other important stuff for ruler
struct gl{
	int count;
	double coordx[100];
	double coordy[100];
	int toggledDraw;
	int toggledFind;
	int toggledGrey;
	int toggledRuler;
	int scale;
} glob;

//our image surface
cairo_surface_t *image; 


static void do_drawing(cairo_t *);

//calculate pixel length
double pixlength(double xo, double yo, double x, double y){
	double distance = sqrt((xo-x)*(xo-x)+(yo-y)*(yo-y));
	return distance;

}

//draws the lines and stuff
static void do_drawing(cairo_t *cr){
	//cairo context setup image
  image = cairo_image_surface_create_from_png("nw.png");
  
  //Scale Image
  
      // calculate proportional scaling
    int img_height = cairo_image_surface_get_height(image);
    int img_width = cairo_image_surface_get_width(image);
    float width_ratio = 600 / float(img_width);
    float height_ratio = 600 / float(img_height);
    float scale_xy = min(height_ratio, width_ratio);
    // scale image and add it
    cairo_save(cr);
    cairo_translate(cr,0,0);
    cairo_matrix_t matrix;
    cairo_matrix_init_scale (&matrix, 1/scale_xy, 1/scale_xy);
    cairo_pattern_t* pattern = cairo_pattern_create_for_surface(image);
    cairo_pattern_set_matrix(pattern, &matrix);
    cairo_pattern_set_filter(pattern, CAIRO_FILTER_GAUSSIAN);
  //cairo_scale(cr, scale_xy, scale_xy);
cairo_set_source(cr, pattern);
//     cairo_set_source_surface(cr, image, 0, 0);
  cairo_paint(cr);

  if(glob.toggledRuler){
	//line settings
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);
	cairo_set_line_width(cr, 2);

	//line stuff
	int i, j;
	for (i = 0; i <= glob.count - 1; i++ ) {
		for (j = 0; j <= glob.count - 1; j++ ) {
		cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
		cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
		}
	}
  
	glob.count = 0;
	cairo_stroke(cr);

	//pixel print

	cairo_move_to(cr, glob.coordx[0], glob.coordy[0]);
	cairo_set_font_size(cr, 16);
	double distance= pixlength(glob.coordx[0], glob.coordy[0], glob.coordx[1], glob.coordy[1]);

	char pixInfo[30];
	
	if (glob.scale == 0){
	  sprintf(pixInfo, "%.2f Pixels", distance); 
	}else{
	  sprintf(pixInfo, "%.2f nm", distance/glob.scale); 
	}
	cairo_select_font_face(cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
	cairo_show_text(cr, pixInfo );
	//cairo_stroke(cr); 
  }
}

#endif
