//A header file for functions implemented when certain signals are sent.

#ifndef SIGNALCALLBACKS_H
#define SIGNALCALLBACKS_H
#include <iostream>
#include <gtk/gtk.h>
#include <cairo.h>
using namespace std;

static gboolean clicked(GtkWidget*, GdkEventButton *, gpointer);

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

void scaleEdited(GtkWidget*, GdkEventButton *, gpointer);

void clickedDraw(GtkWidget*, GdkEventButton *, gpointer);

void clickedFind(GtkWidget*, GdkEventButton *, gpointer);

void clickedGrey(GtkWidget*, GdkEventButton *, gpointer);

void clickedRuler(GtkWidget*, GdkEventButton *, gpointer);

#endif