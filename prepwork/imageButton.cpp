//Testing a pushButton

#include <iostream>
#include <gtk/gtk.h>
#include <cmath>

//(signal giver, what button press/release event triggered, data)
void ClickCallback(GtkWidget *widget, GdkEventButton *event, gpointer callback_data);

static void cb_image_annotate(GtkWidget *imgv,GdkPixbuf *pixbuf,gint shift_x,gint shift_y,gdouble scale_x,gdouble scale_y,gpointer user_data);
GdkPixbuf * grayPixbuf_FromColor(GdkPixbuf *);

static void destroy_event(GtkWidget *widget, gpointer data);
static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);

int main(int argc,char* argv[]){
  //Initialize GTK
  gtk_init(&argc, &argv);
  
  //Declare pointers to widgetssss
  GtkWidget *window, *button, *image_viewer;
  char* filename = "Image1.jpg";
  
  GdkPixbuf *colorPixbuf, *greyPixbuf;
  GtkImage* greyImage, colorImage;
  colorPixbuf = gdk_pixbuf_new_from_file(filename, FALSE);
/*  greyPixbuf = greyPixbuf_FromColor(colorPixbuf);*/
  //gtk_image_set_from_pixbuf((GtkImage*) greyImage, greyPixbuf);
  //gtk_image_set_from_pixbuf((GtkImage*) colorImage, colorPixbuf);
  
  //Declare them widgizzles.
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, FALSE);
  gtk_window_set_title (GTK_WINDOW (window), filename);
  g_signal_connect(window, "destroy", G_CALLBACK(gtk_exit),NULL);
  
  image_viewer = gtk_image_new_from_pixbuf(colorPixbuf);
  button = gtk_button_new_with_label("DON'T PANIC.");
  
  //Connect Callback
  g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(ClickCallback), NULL);
  //( relevant widge, signal to respond to, function to call)
  
  //Throw that widge atta window
  GtkWidget *box1;
  box1 = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX (box1), image_viewer, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX (box1), button, FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(window), box1);
  
  //View that window
  gtk_widget_show( box1);
  gtk_widget_show(image_viewer);
  gtk_widget_show(button);
  gtk_widget_show_all(window);
  
  //Set up Shut down
  g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy_event), NULL);
  
  //wait for user input
  gtk_main();
  
  return 0;
}

void ClickCallback(GtkWidget *widget, GdkEventButton *event, gpointer callback_data){
    // show which button was clicked
    std::cerr << "button pressed: " << event->button << std::endl;
  }
  
static void destroy_event(GtkWidget *widget, gpointer data){
    gtk_main_quit();
  }

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data){
    return FALSE; // must return false to trigger destroy event for window
  }

// GdkPixbuf * grayPixbuf_FromColor(GdkPixBuf *){
// }