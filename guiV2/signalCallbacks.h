//A header file for functions implemented when certain signals are sent.

#ifndef SIGNALCALLBACKS_H
#define SIGNALCALLBACKS_H
#include <iostream>
#include <gtk/gtk.h>
#include <cairo.h>
using namespace std;

//handles mouse click events
void clicked(GtkWidget*, GdkEventButton *, gpointer);

//handles expose events
void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

//scales image
void scaleEdited(GtkWidget*, GdkEventButton *, gpointer);

//updates toggles status
void clickedDraw(GtkWidget*, GdkEventButton *, gpointer);

//updates toggle status
void clickedRuler(GtkWidget*, GdkEventButton *, gpointer);

void clearScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data);

	
//saves the drawings on the screen
void saveScreen(GtkWidget *widget, GdkEventButton *event, gpointer callback_data);



#endif
